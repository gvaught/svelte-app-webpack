const path = require('path')
const webpack = require('webpack')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = {
  name: 'svelte-toolkit',
  mode: 'development',
  entry: './src/index.js',
  output: { filename: '[name].js' },
  optimization: {
    splitChunks: {
      name: 'vendor',
      minChunks: Infinity,
    },
  },
  module: {
    rules: [
      {
        test: /\.(html|svelte)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'svelte-loader',
            options: {
              hotReload: true,
            },
          },
        ],
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              // only enable hot in development
              hmr: process.env.NODE_ENV === 'development',
              // if hmr does not work, this is a forceful method.
              reloadAll: true,
              sourceMap: true,
              fallback: 'style-loader',
            },
          },
          'css-loader',
        ],
      },
    ],
  },
  resolve: {
    // see below for an explanation
    alias: {
      svelte: path.resolve('node_modules', 'svelte'),
    },
    extensions: ['.mjs', '.js', '.svelte'],
    mainFields: ['svelte', 'browser', 'module', 'main'],
  },
  devServer: {
    contentBase: path.join(__dirname, './public'),
    port: 3000,
    publicPath: 'http://localhost:4000/dist/',
    allowedHosts: ['localhost', '.ngrok.io'],
    // hotOnly: true
  },
  plugins: [
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: '[name].css',
      chunkFilename: '[id].css',
    }),
    new webpack.HotModuleReplacementPlugin(),
  ],
}
